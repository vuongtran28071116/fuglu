#####################
# NEW FUGLU RELEASE #
#####################


documentation
-------------
sphinx setup: pip2.7 install sphinx

 * update changelog
 * update default config

fuglu_conf -m all > conf/fuglu.conf.dist
(commit to git)

Make sure all tests pass
------------------------
cd fuglu/tests
nosetests-2.7 unit

#start clamd/spamassassin first
nosetests-2.7 integration

fix code style
..............
autopep8-python2 -i -r .

deploy
------
* update version in fuglu/__init__.py, commit to git

* tag release
git tag -a 0.8.0 -m 'tag 0.8.0'
git push --tags origin master

* build
python setup.py sdist

* pypi upload
twine upload dist/fuglu-<new-version>.tar.gz

* mailing list announcement
